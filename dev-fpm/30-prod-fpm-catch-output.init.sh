# dev-fpm/30-prod-fpm-catch-output.init.sh
# Pass workers output to the STDOUT of the process manager

for PHP_FPM_POOL in /etc/php/*/fpm/pool.d/*; do
  sed -i 's/;catch_workers_output.*/catch_workers_output = yes/' "$PHP_FPM_POOL"
  sed -i 's/;decorate_workers_output.*/decorate_workers_output = no/' "$PHP_FPM_POOL"
done
