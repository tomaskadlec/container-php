# dev-fpm/Dockerfile.tpl
# Install and configure PHP-FPM ${version} image for development
# ${donotedit}

FROM registry.gitlab.com/tomaskadlec/container-php/${image}:${codename}-dev

# install php-fpm
RUN \
    apt update && \
    apt-get install --no-install-recommends -y \
	    php${version}-fpm && \
    rm -rf /var/lib/apt/lists/* && \
    apt clean

# configuration
RUN mkdir -p /run/php && \
    sed -i 's/gosu[[:space:]]\+[^[:space:]]\+[[:space:]]//' /usr/local/container_init/init.sh

# container init
ADD ["*.init.sh", "/usr/local/container_init/"]

# runtime
EXPOSE 9000
CMD ["php-fpm${version}", "--allow-to-run-as-root", "-F", "-y", "/etc/php/${version}/fpm/php-fpm.conf", "-c", "/etc/php/${version}/fpm/"]
