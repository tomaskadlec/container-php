#!/bin/bash
# Run cron on foreground

chown -R root: /var/spool/cron/crontabs
chmod 1730 /var/spool/cron/crontabs
exec /usr/sbin/cron -f
