# prod-cron/Dockerfile.tpl
# Install and configure cron to use with PHP ${version} image for production
# ${donotedit}

FROM registry.gitlab.com/tomaskadlec/container-php/${image}:${codename}-prod

# install cron
RUN \
    apt update && \
    if [ "$codename" = 'stretch' ]; \
        then apt install --no-install-recommends -y cron ssmtp; \
        else apt install --no-install-recommends -y cron msmtp; \
    fi && \
    rm -rf /var/lib/apt/lists/* && \
    apt clean

# configure
ADD ["cron.sh", "/usr/local/container_init/"]
RUN chmod +x "/usr/local/container_init/"

# runtime
ENTRYPOINT ["/bin/bash", "/usr/local/container_init/cron.sh"]
