Prod-cron image
===============

.. _Dockerfile: Dockerfile.tpl

The prod-cron image is used in the production environment mostly. It is suitable
for running periodically recurring tasks. Look into Dockerfile_ for a list of
installed PHP extensions and other packages. Mount your crontabs to
`/var/spool/cron/crontabs` (owned by `root:root`).

When container is started `php-fpm`_ is started automatically. Master process
runs as ``root`` (thus container_init script is altered in Dockerfile_) but
child processes run as ``CONTAINER_UID``/``CONTAINER_GID`` (see `base image`_
documentation).

.. vim: spelllang=en spell textwidth=80 fo-=l: 
.. reformat paragraphs to 80 characters: select using v then hit gq
