#!/bin/bash
# Starts a container for current user

while getopts :p:h OPT; do
    case "$OPT" in
        p)
            PUBLISH="8000:$OPTARG"
            ;;
        *)
            usage
            exit 1
            ;;        
    esac
    shift $((OPTIND-1))
done

# if PUBLISH is empty, set default
if [ -z "$PUBLISH" ]; then
    PUBLISH="8000:8000"
fi    

WORKDIR="${1:-$PWD}"

docker run -ti --rm \
    --env "CONTAINER_UID=$(id -u)" \
    --env "CONTAINER_GID=$(id -g)" \
    --volume="/home/$USER:/home/$USER" \
    --env="DISPLAY" \
    --workdir="$WORKDIR" \
    --expose="8000" \
    --publish="$PUBLISH" \
    registry.gitlab.com/tomaskadlec/container-php:dev
