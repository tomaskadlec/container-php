# prod-fpm/Dockerfile.tpl
# Install and configure PHP-FPM ${version} image for production
# ${donotedit}

FROM registry.gitlab.com/tomaskadlec/container-php/${image}:${codename}-prod

# install libevent support
RUN \
    apt update && \
    apt install --no-install-recommends -y \
        build-essential \
        libevent-dev \
        php-pear \
	    php${version}-dev && \
    pecl install event && \
    apt purge --autoremove -y build-essential php-pear php${version}-dev && \
    rm -rf /var/lib/apt/lists/* && \
    apt clean

# php configuration (cli only)
ADD ["*.ini", "/etc/php/${version}/mods-available/"]
RUN \
    ln -snf "/etc/php/${version}/mods-available/event.ini" "/etc/php/${version}/cli/conf.d/10-event.ini"

# container init
ADD ["*.init.sh", "/usr/local/container_init/"]

