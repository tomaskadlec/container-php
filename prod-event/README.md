# Prod-event image

The prod-event image is used mostly for user testing and production. It provides
access to efficient event handling mechanisms (i.e. `epoll`) and it is suitable
for use with ReactPHP and alternatives.

