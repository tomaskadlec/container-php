#!/bin/sh
# Installs latest version of Composer

cd /tmp

EXPECTED_SIGNATURE=$(wget -q -O - https://composer.github.io/installer.sig)
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
ACTUAL_SIGNATURE=$(php -r "echo hash_file('SHA384', 'composer-setup.php');")

if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
then
    >&2 echo 'ERROR: Invalid installer signature'
    rm composer-setup.php
    exit 1
fi

php composer-setup.php --quiet
RESULT=$?

if [ $RESULT -eq 0 -a -f composer.phar ]; then
    mv composer.phar /usr/local/bin &&
        chmod +x /usr/local/bin/composer.phar &&
        ln -s /usr/local/bin/composer.phar /usr/local/bin/composer
    RESULT="$?"
fi

rm composer-setup.php
exit $RESULT
