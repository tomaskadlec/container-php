# dev/Dockerfile.tpl
# Install developer tools and configure PHP ${version} image for development
# ${donotedit}

FROM registry.gitlab.com/tomaskadlec/container-php/${image}:${codename}-base

# install developer tools and debugging extensions
RUN \
    apt update && \
    apt-get install --no-install-recommends -y \
        curl \
        git \
        gnupg \
        iproute2 \
        netcat \
        openssh-client \
        php${version}-xdebug && \
    rm -rf /var/lib/apt/lists/* && \
    apt clean

# configure xdebug
ADD ["xdebug-config.3x.ini", "/etc/php/${version}/mods-available/xdebug-config.ini"]

ADD ["*.init.sh", "/usr/local/container_init/"]
ENV XDEBUG_CONFIG="idekey=phpstorm"
ENV PHP_IDE_CONFIG="serverName=docker"

# download composer and symfony
ADD ["get*.sh", "/usr/local/bin/"]
RUN /usr/local/bin/getcomposer.sh && \
    /usr/local/bin/getsymfony.sh

RUN \
    apt update && \
    apt install --no-install-recommends -y \
        build-essential \
        libevent-dev \
        php-pear \
        php${version}-dev && \
    pecl install event && \
    apt purge -y --autoremove build-essential php-pear php${version}-dev && \
    rm -rf /var/lib/apt/lists/* && \
    apt clean

ADD ["event.ini", "/etc/php/${version}/mods-available/event.ini"]

RUN ln -snf "/etc/php/${version}/mods-available/event.ini" "/etc/php/${version}/cli/conf.d/10-event.ini"

