#!/bin/sh
# Installs latest version of Symfony installer

wget -O /usr/local/bin/symfony https://get.symfony.com/cli/installer &&
    chmod a+x /usr/local/bin/symfony
