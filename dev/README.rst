Dev image
=========

.. _Dockerfile: Dockerfile.tpl

The dev image is used for development and also for building applications. Look
into Dockerfile_ for a list of installed PHP extensions and other packages.

Node.js
-------

.. _`Nodesource`: https://github.com/nodesource/distributions

The image contains `Node.js`_ in the latest version. It is installed using
Nodesource_ repository. 

Node.js may be necessary to minify and/or obfuscate CSS and JavaScript.

Composer and Symfony
--------------------

.. _Composer: https://getcomposer.org/
.. _Symfony: http://symfony.com/download

Latest versions of Composer_ and Symfony_ installer are installed in the image
and are in ``PATH`` (in ``/usr/local/bin``). Invoke them just using ``composer``
or ``symfony`` commands.

Xdebug
------

.. _`Xdebug`: https://xdebug.org/
.. _`xdebug-config.ini`: xdebug-config.ini

The image contains Xdebug_. It is enabled for all installed SAPIs (in runtime).
Refer to `xdebug-config.ini`_ and Variables section for configuration details.

Variables
---------

``PHP_LIMIT_MEMORY``
    Sets memory limit. Defaults to 1024 MB.

``XDEBUG_REMOTE_HOST``
    Remote host to connect to. Defaults to container default gateway (docker
    host).

``XDEBUG_NESTING_LEVEL``
    Limits recursion to the specified level. Defaults to 512.

.. vim: spelllang=en spell textwidth=80 fo-=l: 
.. reformat paragraphs to 80 characters: select using v then hit gq
