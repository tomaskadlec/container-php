# dev/20-dev-variables.init.sh
# Handles replacements (variables) for all SAPIs in php.ini

declare -A REPLACEMENTS
REPLACEMENTS=(
    [memory_limit]="${PHP_LIMIT_MEMORY:-1024M}"
)

for PHP_INI_DIR in /etc/php/*/{cgi,cli,fpm,apache2}; do
    [ -d "$PHP_INI_DIR" ] || continue
    replace "$PHP_INI_DIR/php.ini" "$(declare -p REPLACEMENTS)" '^[;]\?' '[[:space:]]*=.*$' ' = '
done
