# dev/20-dev-xdebug.init.sh
# Sets limits for debugging, enables Xdebug for all SAPIs

if [ -z "$XDEBUG_REMOTE_HOST" ]; then
    # use default gateway
    XDEBUG_REMOTE_HOST=$(ip route | grep default | head -1 | cut -d\  -f 3)
fi

declare -A REPLACEMENTS
REPLACEMENTS=(
    [xdebug.max_nesting_level]="${XDEBUG_NESTING_LEVEL:-1024}"
    [xdebug.client_host]="${XDEBUG_REMOTE_HOST}"
)

for PHP_INI_DIR in /etc/php/*/; do
    VERSION=$(echo "$PHP_INI_DIR" | cut -d/ -f4)
    replace "/etc/php/$VERSION/mods-available/xdebug-config.ini" "$(declare -p REPLACEMENTS)" '^[;]\?' '[[:space:]]*=.*$' ' = '
    for SAPI in cgi cli fpm apache2; do
        [ -d "$PHP_INI_DIR/$SAPI" ] || continue
        ln -snf "/etc/php/$VERSION/mods-available/xdebug-config.ini" "$PHP_INI_DIR/$SAPI/conf.d/30-xdebug-config.ini"
    done
done
