Prod-fpm image
==============

.. _Dockerfile: Dockerfile

The prod-fpm image is used mostly for user testing and production.  Look into
Dockerfile_ for a list of installed PHP extensions and other packages.

When container is started `php-fpm`_ is started automatically. Master process
runs as ``root`` (thus container_init script is altered in Dockerfile_) but
child processes run as ``CONTAINER_UID``/``CONTAINER_GID`` (see `base image`_
documentation).

`php-fpm`_ listens on tcp/9000, port is exposed to the Docker host. This value
is hardcoded and cannot be changed.

`php_fpm` forwards logs to ``strerr``.

.. vim: spelllang=en spell textwidth=80 fo-=l: 
.. reformat paragraphs to 80 characters: select using v then hit gq
