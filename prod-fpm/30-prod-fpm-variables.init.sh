# dev-fpm/30-dev-fpm-variables.sh
# Handles replacements (variables) for php-fpm.conf

declare -A REPLACEMENTS
REPLACEMENTS=(
    [listen]="9000"
    [error_log]="/proc/self/fd/2"
    [access_log]="/proc/self/fd/2"
    [user]="${CONTAINER_USER}"
    [group]="${CONTAINER_GROUP}"
)

for PHP_INI_DIR in /etc/php/*/fpm; do
    # php-fpm.conf
    if [ -f "$PHP_INI_DIR/php-fpm.conf" ]; then
        replace "$PHP_INI_DIR/php-fpm.conf" "$(declare -p REPLACEMENTS)" '^' '[[:space:]]*=.*$' ' = '
    fi
    # pool.d/www.conf
    if [ -f "$PHP_INI_DIR/pool.d/www.conf" ]; then
        replace "$PHP_INI_DIR/pool.d/www.conf" "$(declare -p REPLACEMENTS)" '^' '[[:space:]]*=.*$' ' = '
    fi
done
