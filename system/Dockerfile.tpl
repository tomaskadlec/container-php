# debian-stretch/Dockerfile
# Install commonly used tools, add container init and configure image
# ${donotedit}

FROM "${system}"

# install commonly used tools
RUN \
    apt-get update && \
    apt-get full-upgrade -y && \
    apt-get install --no-install-recommends -y \
        apt-transport-https \
        curl \
        gnupg \
        gosu \
        less \
        lsb-release ca-certificates \
        procps \
        wget && \
    rm -rf /var/lib/apt/lists/* && \
    apt clean

# add deb.sury.org repository
RUN \
    [ `lsb_release -si` = 'Debian' ] || exit 0; \
    wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg && \
    sh -c 'echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'

RUN \
    [ `lsb_release -si` = 'Ubuntu' ] || exit 0; \
    apt update && \
    apt-get install -y software-properties-common && \
    add-apt-repository ppa:ondrej/php && \
    rm -rf /var/lib/apt/lists/* && \
    apt clean

# www-directory
RUN mkdir /var/www && chown -R 'www-data:' /var/www

# container init
ENV INIT_DIR=/usr/local/container_init
ADD ["init.sh", "*.init.sh", "/usr/local/container_init/"]
RUN chmod +x /usr/local/container_init/init.sh

# timezone
ENV TZ=Europe/Prague
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# runtime configuration
EXPOSE 8000
ENTRYPOINT ["/bin/bash", "/usr/local/container_init/init.sh"]
CMD ["/bin/bash"]
