#!/bin/bash -e

MODE='generate'

PHP_VERSIONS=(
    '7.4:jammy'
    '8.3:jammy'
)

PHP_FLAVORS=(
    'base'
    'dev'
    'dev-fpm'
    'prod'
    'prod-fpm'
    'prod-cron'
    'prod-event'
)

SYSTEMS=(
    "ubuntu:jammy"
)

##
# Build base system image
function build_system {
    export system="$1"
    export codename="${system##*:}"
    dockerfile="system/Dockerfile.${codename}"

    echo "==> Building ${system}"
    cat "system/Dockerfile.tpl" | envsubst '$system:$codename' > "$dockerfile"
    cat >> .gitlab-ci.yml << -CI-
${codename}:
    stage: system
    only:
        - master
    script:
        - docker build --file "$dockerfile" --tag registry.gitlab.com/tomaskadlec/container-php/${system} system/
        - docker push registry.gitlab.com/tomaskadlec/container-php/${system}

-CI-

    if [ "$MODE" = 'build' ]; then
        # build common image
        docker pull "$SYSTEM"
        docker build --file "$dockerfile" --tag "registry.gitlab.com/tomaskadlec/container-php/${system}" "system/"
    fi
}

##
# Build image with PHP
function build_image {
    export system="$1"
    export codename="${system##*:}"
    export flavor="$2"
    export version="$3"
    export donotedit="This file was created automatically. Do not edit by hand!"
    export image="php${version/./}"
    export dockerfile="$flavor/Dockerfile.${codename}.${image}"

    echo "==> Building $codename $flavor $version"
    cat "$flavor/Dockerfile.tpl" | envsubst '$system:$codename:$flavor:$version:$image:$donotedit' > "$dockerfile"
    if [ "$MODE" = 'build' ]; then
        docker build --file "$dockerfile" --tag "registry.gitlab.com/tomaskadlec/container-php/${image}:${codename}-${flavor}" "${flavor}/"
    fi
    cat >> .gitlab-ci.yml << -CI-
${codename}_${flavor}_${image}:
    stage: ${flavor}
    only:
        - master
    script:
        - docker build --file "$dockerfile" --tag "registry.gitlab.com/tomaskadlec/container-php/${image}:${codename}-${flavor}" "${flavor}/"
        - docker push "registry.gitlab.com/tomaskadlec/container-php/${image}:${codename}-${flavor}"
-CI-

    if [[ "$codename" == 'stretch' && "$version" =~ 5.6|7.0|7.1|7.2 ]]; then
        cat >> .gitlab-ci.yml << -CI-
        - docker tag "registry.gitlab.com/tomaskadlec/container-php/${image}:${codename}-${flavor}" "registry.gitlab.com/tomaskadlec/container-php/${image}:${flavor}"
        - docker push "registry.gitlab.com/tomaskadlec/container-php/${image}:${flavor}"
-CI-
    fi

cat >> .gitlab-ci.yml << -CI-

-CI-
}

OPTS=`getopt -o b --long build -n 'parse-options' -- "$@"`

if [ $? != 0 ] ; then echo "Failed parsing options." >&2 ; exit 1 ; fi

eval set -- "$OPTS"

while true; do
  case "$1" in
    -b | --build)
        MODE='build'
		shift
		;;
    * )
        break;
		;;
  esac
done

shift $((OPTIND))

# recreate .gitlab-ci.yml
cat .gitlab-ci.yml.tpl > .gitlab-ci.yml

# build base system images
for SYSTEM in "${SYSTEMS[@]}"; do
    build_system "$SYSTEM"
done

# build different flavors
for SYSTEM in "${SYSTEMS[@]}"; do
    CODENAME="${SYSTEM##*:}"
    for PHP_FLAVOR in "${PHP_FLAVORS[@]}"; do
        for PHP_VERSION in "${PHP_VERSIONS[@]}"; do
            VERSION="${PHP_VERSION%%:*}"
            if echo "$PHP_VERSION" | grep -vqs "$CODENAME"; then
                echo "==> Skipping ${CODENAME} ${PHP_FLAVOR} ${VERSION}"
                continue
            fi
            build_image "$SYSTEM" "$PHP_FLAVOR" "$VERSION";
        done
    done
done

if [ "$BUILD" = 'build' ]; then
    # remove dangling images
    docker images --quiet --filter=dangling=true | xargs --no-run-if-empty docker rmi
fi
