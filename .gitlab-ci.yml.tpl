# https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker
default:
    image: docker:latest
    services:
        - docker:dind
    before_script:
       - docker info
       - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"

variables:
    DOCKER_TLS_CERTDIR: "/certs"

stages:
    - system
    - base
    - dev
    - dev-fpm
    - prod
    - prod-fpm
    - prod-event
    - prod-cron

