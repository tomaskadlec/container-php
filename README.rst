container-php
=============

.. _`base`: base/README.rst
.. _`dev`: dev/README.rst
.. _`dev-fpm`: dev-fpm/README.rst
.. _`prod`: prod/README.rst
.. _`prod-fpm`: prod-fpm/README.rst
.. _`prod-cron`: prod-cron/README.rst

.. _`Debian 9 Stretch`: https://wiki.debian.org/DebianStretch
.. _`Debian 10 Buster`: https://wiki.debian.org/DebianBuster
.. _`PHP`: http://www.php.net
.. _`PHP CLI`: http://php.net/manual/en/features.commandline.php
.. _`Composer`: http://getcomposer.org
.. _`Symfony installer`: http://symfony.com/download
.. _`Node.js`: https://nodejs.org/

This project builds `Debian 9 Stretch`_ and `Debian 10 Buster`_ based images with PHP_.
It builds different images for different purposes. Look to their own README files for
further information.

`base`_
    Contains PHP CLI and extensions commonly used. It is a starting point for
    building subsequent images. 

`dev`_
    It is a image designed for development - Xdebug is installed and enabled.
    The image also contains tools for building production applicatons (e.g.
    Composer_ `Symfony installer`_, nodejs_).

`dev-fpm`_
    This is a development image which contains `php-fpm`_ so it can be used as
    an application server. Intended use is for testing purposes with extended 
    login support and Xdebug enabled.

`prod`_
    This image is configured for production use.

`prod-fpm`_
    A production image with php-fpm configured and running.

`prod-cron`_
    A production image with php and cron configured and running.

License
-------

.. _`The MIT License`: LICENSE

This project is licensed under `The MIT License`_.

.. vim: spelllang=en spell textwidth=80 fo-=l: 
.. reformat paragraphs to 80 characters: select using v then hit gq
