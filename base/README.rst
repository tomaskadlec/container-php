Base image
==========

.. Dockerfile_: Dockerfile.tpl

The base image used in later steps. Look into Dockerfile_ for a list of
installed PHP extensions and other packages.

container_init
--------------

.. _`init.sh`: init.sh
.. _`ENTRYPOINT`: https://docs.docker.com/engine/reference/builder/#/entrypoint
.. _`CMD`: https://docs.docker.com/engine/reference/builder/#/cmd

*container_init* is a shell script (`init.sh`_) that is registered as an
ENTRYPOINT_. It does runtime initialization of the container before requested
process is executed. It takes following steps:

#. It sources additional scripts (files matching
   ``/usr/local/container_init/*.init.sh``) that actually alter the running
   container.

   One use is to alter configuration using environment variables as described in
   section Variables of this document. But it may be used for other purposes as
   needed.

#. It runs a process using command provided to ``docker run``. It is always
   executed using ``exec`` thus the process has PID 1.

   If no command was specified it defaults to value set by CMD_ in Dockerfile_,
   e.g. ``["/bin/bash"]``.

   The command is always executed using as specified UID/GID or
   ``www-data:www-data`` if none given specified. 

Please refer to the following section for configuration variables.

Variables
---------

``PHP_TIMEZONE``
    Sets timezone for all installed SAPIs. Defaults to ``Europe/Prague``.

``PHP_LIMIT_EXECUTION``
    Sets execution time limit for all installed SAPIs (except CLI it's not
    possible). Defaults to 60 seconds.

``PHP_LIMIT_MEMORY``
    Sets memory limit. Defaults to 256 MB.

``CONTAINER_GID``
    GID that should be used in container for running the command. Defaults
    to GIT of ``www-data`` group. If given GID does not exist a new group is
    created inside the container (called ``container_group``).

``CONTAINER_UID``
    UID that should be used in container for running the command. Defaults to
    UID of ``www-data`` user. If given UID does not exist a new user is created
    inside the container (called ``container_user``).

.. vim: spelllang=en spell textwidth=80 fo-=l: 
.. reformat paragraphs to 80 characters: select using v then hit gq
