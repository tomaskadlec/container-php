# base/Dockerfile.tpl
# Install PHP ${version}
# ${donotedit}

FROM registry.gitlab.com/tomaskadlec/container-php/${system}

ENV PHP_VERSION="${version}"

RUN \
    apt update && \
    apt-get install --no-install-recommends -y \
    php${version}-amqp \
    php${version}-bcmath \
    php${version}-cli \
    php${version}-common \
    php${version}-curl \
    php${version}-gd \
    php${version}-intl \
    php${version}-ldap \
    php${version}-mbstring \
    php${version}-memcache \
    php${version}-mongodb \
    php${version}-mysql \
    php${version}-pgsql \
    php${version}-readline \
    php${version}-redis \
    php${version}-soap \
    php${version}-sqlite3 \
    php${version}-xml \
    php${version}-zip \
    unzip && \
    rm -rf /var/lib/apt/lists/* && \
    apt clean

RUN \
    [ "${version}" == "7.4" ] || exit 0; \
    apt update && \
    apt-get install --no-install-recommends -y \
    php${version}-json && \
    rm -rf /var/lib/apt/lists/* && \
    apt clean

ADD ["*.init.sh", "/usr/local/container_init/"]
