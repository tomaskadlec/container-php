# base/init.d/10-base-replacements.sh
# Basic replacements in all php.ini files found

declare -A REPLACEMENTS
REPLACEMENTS=(
    [max_execution_time]="${PHP_LIMIT_EXECUTION:-60}"
    [memory_limit]="${PHP_LIMIT_MEMORY:-256M}"
    [date.timezone]="${PHP_TIMEZONE:-Europe/Prague}"
    [upload_max_filesize]="${PHP_UPLOAD_MAX_FILESIZE:-16M}"
    [post_max_size]="${PHP_POST_MAX_SIZE:-16M}"
    [session.gc_maxlifetime ]="${PHP_SESSION_GC_MAXLIFETIME:-86400}"
)

for PHP_INI_DIR in /etc/php/*/{cgi,cli,fpm,apache2}; do
    [ -d "$PHP_INI_DIR" ] || continue
    replace "$PHP_INI_DIR/php.ini" "$(declare -p REPLACEMENTS)" '^[;]\?' '[[:space:]]*=.*$' ' = '
done
