Prod image
==========

.. _`Dockerfile`: Dockerfile.tpl
.. _`Xdebug`: https://xdebug.org
.. _`Node.js`: https://nodejs.org
.. _`Composer`: http://getcomposer.org
.. _`dev image`: ../dev/README.rst

The prod image is used for testing and benchmarks. Look into Dockerfile_ for a
list of installed PHP extensions and other packages. It does not contain
`Xdebug`_, `Node.js`_ or `Composer`_. Applications must be prebuilt using `dev
image`_

Variables
---------

``PHP_LIMIT_MEMORY``
    Sets memory limit. Defaults to 512 MB.

.. vim: spelllang=en spell textwidth=80 fo-=l: 
.. reformat paragraphs to 80 characters: select using v then hit gq
