# prod/Dockerfile.tpl
# Configure PHP ${version} image for production
# ${donotedit}

FROM registry.gitlab.com/tomaskadlec/container-php/${image}:${codename}-base

# install production tools and extensions
#RUN \
#    apt update && \
#    apt-get install --no-install-recommends -y \
#        FIXME && \
#    rm -rf /var/lib/apt/lists/* && \
#    apt clean

# container init
ADD ["*.init.sh", "/usr/local/container_init/"]

ENV SYMFONY_ENV=prod
